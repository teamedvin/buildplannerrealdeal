//
//  MomentsTwoWeeksGantView.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-12.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class MomentsTwoWeeksGantView: UIView {

    var dates = [NSDictionary]()
    var moments = [NSDictionary]()
    var machines = [NSDictionary]()
    var dayWidth: CGFloat = 0.0
    

    override func drawRect(rect: CGRect) {

       
        dayWidth = bounds.size.width/10
        let yContext = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(yContext, UIColor.whiteColor().CGColor)
        
        CGContextMoveToPoint(yContext, 0 , (bounds.size.height/8))
        CGContextAddLineToPoint(yContext, 0, bounds.size.height)
        
        CGContextStrokePath(yContext)
        
        let xContext = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(xContext, UIColor.whiteColor().CGColor)
        
        CGContextMoveToPoint(xContext, 0 , (bounds.size.height/8))
        CGContextAddLineToPoint(xContext, bounds.size.width, (bounds.size.height/8))
        
        CGContextStrokePath(xContext)
        
        drawLineLoop()
        addTimeRect()
        
            }
    
    func addTimeRect(){
    
        var date:Double =  Double(bounds.size.height/7)
        
        for i in 0..<moments.count {
            let startDate : String = moments[i]["start"] as! String
            let timeToStart : Int = daysLeftFromToday(startDate)
            let startMoment = Double(timeToStart) * Double(dayWidth)
            
            let stopDate : String = moments[i]["end"] as! String
            let duration : Int = weekdaysBetween(stringToDate(startDate), secondDate: stringToDate(stopDate))
            
            let stopMoment = Double(duration) * Double(dayWidth)
            
            
            moments[i].setValue(timeToStart, forKey: "timeToStart")
            moments[i].setValue(duration, forKey: "duration")
            
            let color =  colorWithHexString(moments[i]["color"] as! String).CGColor
            let name = moments[i]["name"] as! String
            
            drawTimeRect(CGFloat(startMoment), startY: CGFloat(date), width: CGFloat(stopMoment), color: color, name: name, index: i)
            
            date+=Double(bounds.size.height/22)
        }
        
        
      
        
    }
    
    func drawTimeRect(startX: CGFloat, startY: CGFloat, width: CGFloat, color: CGColor, name: String, index : Int){
        
       
        
        let button = UIButton();
        button.setTitle(name, forState: .Normal)
        
        button.frame = CGRect(x: startX - 10, y: startY, width: width,height: 28) // X, Y, width, height
        button.addTarget(self, action: #selector(TwoWeeksGanttView.buttonPressed(_:)), forControlEvents: .TouchUpInside)
        button.backgroundColor = UIColor(CGColor: color)
        button.tag = index
        self.addSubview(button)
    }
    
    func buttonPressed(sender: UIButton!) {
        let name : String = (sender.titleLabel?.text)!
     
        let color : String = moments[sender.tag]["color"] as! String
        
        let DynamicView=UIView(frame: CGRectMake(0, 17, bounds.width, 40))
        DynamicView.backgroundColor = colorWithHexString(color)
        
        self.addSubview(DynamicView)
        
        let label = UILabel(frame: CGRectMake((bounds.size.width/4), 17, (bounds.size.width/2), 40))
        
        label.layer.borderWidth = 0
        label.backgroundColor = UIColor.clearColor()
        label.font = UIFont.systemFontOfSize(20)
        label.textAlignment = NSTextAlignment.Center
        label.text = name
        
        self.addSubview(label)
        
        let imageLeftArrow = UIImage(named: "leftarrow") as UIImage?
        let leftButton1 = UIButton();
        
        leftButton1.contentMode = UIViewContentMode.Center;
        leftButton1.frame = CGRectMake(0, 17, 115, 40)
        leftButton1.addTarget(self, action: #selector(MomentsTwoWeeksGantView.moveAllLeft(_:)), forControlEvents: .TouchUpInside)
        leftButton1.tag = sender.tag
        leftButton1.setImage(imageLeftArrow, forState: .Normal)
       
        self.addSubview(leftButton1)
        
        
        let imageRightArrow = UIImage(named: "rightarrow") as UIImage?
        let rightButton1 = UIButton();
        
        rightButton1.contentMode = UIViewContentMode.Center;
        rightButton1.frame = CGRectMake(bounds.size.width - 120, 17, 115, 40)
        rightButton1.addTarget(self, action: #selector(MomentsTwoWeeksGantView.moveAllRight(_:)), forControlEvents: .TouchUpInside)
        rightButton1.tag = sender.tag
        rightButton1.setImage(imageRightArrow, forState: .Normal)
        self.addSubview(rightButton1)
        print("sender tag = \(sender.tag)")
        
    }
    
    func moveAllLeft(sender : UIButton) {
    
        let key = moments[sender.tag]["key"] as? String
  
        let momentRef = MOMENTS_REF.childByAppendingPath(key)
        let startDate : String = moments[sender.tag]["start"] as! String
     
        let endDate : String = moments[sender.tag]["end"] as! String
        
     
        let start = ["start" : backOneDay(startDate)]
        let end = ["end" : backOneDay(endDate)]
        
     
        momentRef.updateChildValues(start)
        momentRef.updateChildValues(end)
        
        for dict in machines {
            
            if(dict["moment"] as? String == key){
                let key = dict["key"] as! String
                let machineRef = MACHINES_REF.childByAppendingPath(key)
                print(dict["name"] as! String)
                
                machineRef.updateChildValues(start)
                machineRef.updateChildValues(end)
            }
            
        }
      
         NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
    }
    
    func moveAllRight (sender : UIButton){
        let key = moments[sender.tag]["key"] as? String
   
        let momentRef = MOMENTS_REF.childByAppendingPath(key)
        let startDate : String = moments[sender.tag]["start"] as! String

        let endDate : String = moments[sender.tag]["end"] as! String
    
        
        let start = ["start" : addOneDay(startDate)]
        let end = ["end" : addOneDay(endDate)]
        
        
        momentRef.updateChildValues(start)
        momentRef.updateChildValues(end)
       
        for dict in machines {
            
            if(dict["moment"] as? String == key){
                let key = dict["key"] as! String
                let machineRef = MACHINES_REF.childByAppendingPath(key)
                print(dict["name"] as! String)
                
                machineRef.updateChildValues(start)
                machineRef.updateChildValues(end)
            }
            
        }
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
        
    }
  
    
    func drawLineDown(x : CGFloat, view: UIView, startX: CGFloat, date: String, weekday: Int){
        
       
        let label = UILabel(frame: CGRectMake(x-startX + 15, (bounds.size.height/11), 100, 30))
        
        label.font = UIFont.systemFontOfSize(20)
        label.textAlignment = NSTextAlignment.Left
        label.text = date
        
        if weekday == 1 || weekday == 7 {
            label.textColor = UIColor.redColor()
        } else {
            label.textColor = UIColor.whiteColor()
        }
        view.addSubview(label)
        
        let lineContext = UIGraphicsGetCurrentContext()
        
        if weekday == 7 || weekday == 6 || weekday == 1 {
            CGContextSetStrokeColorWithColor(lineContext, UIColor.redColor().CGColor)
            CGContextSetLineWidth(lineContext, 4.0)
            
        } else {
            CGContextSetStrokeColorWithColor(lineContext, UIColor.whiteColor().CGColor)
            CGContextSetLineWidth(lineContext, 1.0)
        }
        
        
        CGContextMoveToPoint(lineContext, x , (bounds.size.height/10))
        CGContextAddLineToPoint(lineContext, x, bounds.size.height)
        
        CGContextStrokePath(lineContext)
        
        
    }
    
    
    func drawLineRight(y : CGFloat){
        
        let lineContext = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(lineContext, UIColor.whiteColor().CGColor)
        
        CGContextMoveToPoint(lineContext, 0 , y)
        CGContextAddLineToPoint(lineContext, bounds.size.width-130, y)
        
        CGContextStrokePath(lineContext)
    }
    
    func drawLineLoop(){
        var i = bounds.size.width/10 - 10
        var dateIndex: Int = 0
        let start = bounds.size.width/10
        
        while i < bounds.size.width {
            let weekday = (dates[dateIndex]["weekDay"] as? Int)!
            if weekday == 7 || weekday == 1 {
                print("no day! weekday")
            } else {
            drawLineDown(CGFloat(i), view: self, startX: start, date: (dates[dateIndex]["date"] as? String)!, weekday:weekday )
            
            i = i + bounds.size.width/10
            
        }
            dateIndex += 1
        }
        
        
        
    }
  
    


}
